(function() {

 
 var githubService=function($http){
 
 var getUser=function(username){
   return  $http.get("https://api.github.com/users/"+username)
            .then(function(response){
              return response.data;
            });
    }

 var getUserRepository=function(user){
   return    $http.get(user.repos_url)
            .then(function(response){
              return response.data;
            });
  }

  var getRepoDetails=function(username,reponame){
    var repo;
    var reopUrl="https://api.github.com/repos/"+username+"/"+reponame;
    
       return $http.get(reopUrl)
            .then(function(response){
              repo=response.data;
              return $http.get(reopUrl+"/collaborators");
              
            })
              .then (function(response){
                repo.collaborators=response.data;
                return repo;
              });
              
            };
    
 return{
 getUser:getUser,
 getUserRepository:getUserRepository,
 getRepoDetails:getRepoDetails
 };
 
 };
 
var app = angular.module("githubViewer");
app.factory("githubService",githubService);

  
}());