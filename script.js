(function() {
 
 var app = angular.module("githubViewer");
 
 //var app = angular.module("githubViewer");

  var MainController = function($scope, githubService) {
    var onUserComplete = function(data) {
      $scope.user = data;
       githubService.getUserRepository($scope.user)
      .then(onReposComplete, onError);
    };
    
    var onReposComplete = function(data){
      $scope.repos = data;  
        
    };
    
    var onError = function(reason) {
      $scope.error = "there is no user with this name";
    };
    
    //$http.get("https://api.github.com/users/robconery")
      //      .then(onUserComplete, onError);
  
    $scope.search=function(username){
          githubService.getUser(username)
            .then(onUserComplete, onError);
      }
    //$scope.username="robconery";
  };
  app.controller("MainController", MainController);
}());