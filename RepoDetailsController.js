(function(){
  
  
   var app = angular.module("githubViewer");
    
   var RepoDetailsController = function($scope,$stateParams,githubService){
      var login=$stateParams.login;
      var repoName=$stateParams.repoName;
     // alert(login +" & "+ repoName);
      
     var onComplete = function(data){
      $scope.repo = data;  
    };
    
    var onError = function(reason) {
      $scope.error = "there is no user with this name";
    };
    
     githubService.getRepoDetails(login,repoName)
      .then(onComplete,onError);
      
    }
  
  app.controller("RepoDetailsController", RepoDetailsController);
     
   
}());