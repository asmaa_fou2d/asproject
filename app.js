(function() {
  
var app = angular.module('githubViewer', ["ui.router"]);

app.config(function($stateProvider, $urlRouterProvider) {
  
    $urlRouterProvider.otherwise('/Home');
    
    $stateProvider.state('Home', {
                          url: '/Home',
                          templateUrl: 'HomePage.html'
                        })
                  .state('User', {
                      url: '/User',
                      templateUrl: 'GetUser.html',
                    
                      })
                  .state('User.UserRepository', {
                      url: '/UserRepository',
                      templateUrl: 'UserRepository.html'
                  })
                  .state('RepoDetails', {
                   url: '/RepoDetails/:login/:repoName',
                   templateUrl: 'RepoDetails.html',
                   controller:'RepoDetailsController'
                  });
});

}());